<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="text"/>

    <xsl:template match="/">
	<xsl:apply-templates select="RESULT/meta[@property='og:image']"/>
	<xsl:apply-templates select="RESULT"/>
    </xsl:template>
    <xsl:template match="meta">
	<xsl:value-of select="./@content"/><xsl:text>&#10;</xsl:text>
    </xsl:template>
    <xsl:template match="RESULT">
	<xsl:value-of select="URL_FLACZIP"/><xsl:text>&#10;</xsl:text>
    </xsl:template>

</xsl:stylesheet>
