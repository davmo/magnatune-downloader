<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <!--

    Report selected data from the Magnatune catalogue as text.

    Invoke as:
        xsltproc - -stringparam code "xxx" parse_album_tracks.xsl ../Magnatune_Data/album_info_xml

    -->

    <xsl:strip-space elements="*"/>
    <xsl:output method="text" omit-xml-declaration="yes" indent="no"/>

    <!-- Define a default for the album code -->
    <xsl:param name="code">abaroque-mozart</xsl:param>

    <xsl:template match="/">
        <xsl:for-each select="AllAlbums/Album">
            <xsl:if test="albumsku = $code">
                <xsl:value-of select="concat('Artist: ',artist,'&#xA;')"/>
                <xsl:value-of select="concat('Album:  ',albumname,'&#xA;')"/>
                <xsl:value-of select="concat('Genres: ',magnatunegenres,'&#xA;')"/>
                <xsl:value-of select="concat('Code:   ',albumsku,'&#xA;')"/>
                <xsl:value-of select="concat('URL:    ','http://magnatune.com/artists/albums/',
                    albumsku,'&#xA;')"/>
                <xsl:text>Tracks:&#xA;</xsl:text>
                <xsl:for-each select="Track">
                    <xsl:value-of select="concat('  ',format-number(tracknum,'00'),
                        ': ',trackname,'&#xA;')"/>
                </xsl:for-each>
                <xsl:value-of select="concat('Notes:  ',album_notes,'&#xA;')"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>
